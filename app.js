const express = require('express');
const path = require('path');
const home = require('./routes/home');
const animalsJwt = require('./routes/animals-jwt');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use((err, req, res, next) => {
  console.log('ERROR: ' + err);
  res.render('error', { message: err.message });
});

app.use('/', home);
app.use('/animals', animalsJwt);

app.listen(5000, () => console.log('Running on port 5000'));
