const express = require('express');
const router = express.Router();
const axios = require('axios');

let data = [];

let auth = {
  username: 'anma',
  password: 'anma',
};

let basicHeader = Buffer.from(`${auth.username}:${auth.password}`).toString(
  'base64'
);

axios
  .get('http://localhost:8080/rest/api/v1/animals', {
    headers: {
      Authorization: `Basic ${basicHeader}`,
    },
  })
  .then((response) => {
    data.push(response.data);
  });

router.get('/', (req, res, next) => {
  console.log(`Getting Data from REST`);
  console.log(data[0]);
  res.render('animals', { animals: data[0] });
});

module.exports = router;
