const express = require('express');
const router = express.Router();

router.get('/', (req, resp, next) => {
  resp.render('home', null);
});

module.exports = router;
