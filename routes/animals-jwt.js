const express = require('express');
const router = express.Router();
const axios = require('axios');

let data = [];

let loginData = {
  username: 'anma',
  password: 'anma',
};

let jwt = '';

function getJwt() {
  console.log('Getting JWT !');
  axios
    .post('http://localhost:8080/auth', {
      username: 'anma',
      password: 'anma',
    })
    .then((response) => {
      jwt = response.data.jwt;
      console.log(jwt);
    })
    .catch((error) => console.log(error));
}

router.get('/', (req, res) => {
  getJwt();
  axios
    .get('http://localhost:8080/rest/api/v1/animals', {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    })
    .then((response) => {
      data.push(response.data);
      console.log(data[0]);
      res.render('animals', { animals: data[0] });
    })
    .catch((error) => console.log(error));
});

module.exports = router;
